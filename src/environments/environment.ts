// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  message: 'devlopment',
  //apiEndPoint: 'http://polls.apiblueprint.org/',
  //apiEndPoint: 'http://127.0.0.1:8080/v1/',
  apiEndPoint: 'https://magnolia-staging-backend.assicurazione-droni.it/v1/',
  
  //apiEndPoint: 'https://testbackend.oil-sorber.com/v1/',
  remoteCouchDB:'https://7d0db823-acc2-4729-8cd1-8d835cdaa94c-bluemix.cloudant.com:443',
  dbUserName:'user1',
  dbUserPassword:'user1',

  sentryEndpoint: 'https://ae328a26606c4216b6cadb711ce08b4a@sentry.io/303099'
  
  // remoteCouchDB:'http://localhost:5984/',
  // dbUserName:'Navaseelan',
  // dbUserPassword:'1qaz!QAZ'



};
