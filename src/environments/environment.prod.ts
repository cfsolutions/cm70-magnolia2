export const environment = {
  production: true,
  message: 'production',
  
  apiEndPoint: 'https://backend.magnolia.campomarzio70.it/v1/',
  
  remoteCouchDB:'https://7d0db823-acc2-4729-8cd1-8d835cdaa94c-bluemix.cloudant.com:443',
  dbUserName:'user1',
  dbUserPassword:'user1',

  sentryEndpoint: 'https://761543091fab4e16a5d083fa1599e0ef@sentry.io/1201881'

};
