export const environment = {
  production: true,
  message: 'staging',
  // apiEndPoint: 'http://polls.apiblueprint.org/',
  apiEndPoint: 'https://magnolia-staging-backend.assicurazione-droni.it/v1/',
  remoteCouchDB:'https://7d0db823-acc2-4729-8cd1-8d835cdaa94c-bluemix.cloudant.com:443',
  dbUserName:'user1',
  dbUserPassword:'user1',
  // remoteCouchDB:'http://localhost:5984/',
  // dbUserName:'Navaseelan',
  // dbUserPassword:'1qaz!QAZ'
  sentryEndpoint: 'https://ae328a26606c4216b6cadb711ce08b4a@sentry.io/303099'

};
