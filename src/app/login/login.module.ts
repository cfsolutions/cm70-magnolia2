import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MaterialInputModule } from '../material-input.module';

import { LoginComponent } from './login.component';
import { LoginService } from './login.service';

const moduleRoutes: Routes = [
    { path: 'customer', children: [
      { path: 'login', component: LoginComponent }
    ]}
  ]

  @NgModule({
    imports: [
      CommonModule,
      RouterModule.forChild(moduleRoutes),
      MaterialInputModule,
    ],
    declarations: [
        LoginComponent
    ],
    providers: [LoginService]
  })

export class LogInModule {

}
