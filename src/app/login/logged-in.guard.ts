import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';

@Injectable()
export class LoggedInGuard implements CanActivate {
    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot) {
        if ((localStorage.getItem('login_status') == null || localStorage.getItem('login_status') === undefined)) {
            // If not, redirect to the login page
            this.router.navigate(['/login']);
            return false;
        } else {
            return true;
        }

    }

}
