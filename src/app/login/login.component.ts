import * as Raven from 'raven-js';  //Error reporting
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './login.service';
import { ToastrService } from 'ngx-toastr';
import { VERSION } from './../../environments/version';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loggedIn = false;
  git_hash = VERSION.hash;

  constructor(private router: Router, private loginService: LoginService, private toastr: ToastrService) { }

  login(event, username, password) {
    event.preventDefault();  
    if (username) {
      debugger
      this.getLoginUserRelatedData(username, password);     
      
    } else {
      localStorage.clear();
      this.loggedIn = false;
      this.router.navigate(['login']);
    }
  }

  isLoggedIn() {
    return this.loggedIn;
  }
  ngOnInit() {
    if (localStorage.getItem('login_status')) {
      this.router.navigate(['/dashboard']);
    }    
  }

  getLoginUserRelatedData(username:any, password:any) {
    this.loginService.getLoginUserRelatedData(username, password)
    .subscribe(result => {
      if (result.status === 200) {
        const userDetails = JSON.parse(result._body);
        localStorage.setItem('customer_table', userDetails.couchDBcustomersDatabase);
        localStorage.setItem('order_table', userDetails.couchDBordersDatabase);

        console.log("customer :"+userDetails.couchDBcustomersDatabase)
        console.log("order :"+userDetails.couchDBordersDatabase)
       debugger
        localStorage.setItem('username', username);
        localStorage.setItem('password', password);  //TODO: This is just for development purpose
        localStorage.setItem('agentCode', userDetails.agentCode)
        localStorage.setItem('isShop', userDetails.isShop);  
        localStorage.setItem('login_status', 'true');  
        
        Raven.setUserContext({
            username: username,
            userDetails: userDetails
        });

        this.router.navigate(['dashboard']);
      }

    },
    error => {
        this.toastr.error('Errore nel login');
    });
  }



}
