import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


import { genId } from '../utils';

@Injectable()
export class LoginService {
  private apiurl = environment.apiEndPoint + 'user';
  constructor(private http: Http) {
  }

  getLoginUserRelatedData(username:any, password:any): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append("Authorization", "Basic " + btoa(username + ":" + password)); 
    // return this.http.get(this.apiurl + '/'+`${username}`, { headers: headers }).map((response: Response) => <any>response)
    // .catch(this.handleError)
    return this.http.get(this.apiurl, { headers: headers }).map((response: Response) => <any>response)
      .catch(this.handleError)
  }
  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}


