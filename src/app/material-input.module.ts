import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatCardModule, MatInputModule, MatSelectModule,
         MatDatepickerModule, MatNativeDateModule, MatIconModule, MatListModule,
         MatCheckboxModule, MatAutocompleteModule, MatTabsModule
       } from '@angular/material';

@NgModule({
  imports: [
    FormsModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatCheckboxModule,
    MatSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatIconModule,
    MatListModule,
    MatAutocompleteModule,
    MatTabsModule
  ],
  exports: [
    FormsModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatCheckboxModule,
    MatSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatIconModule,
    MatListModule,
    MatAutocompleteModule,
    MatTabsModule
  ],
  providers: [],
})
export class MaterialInputModule {}