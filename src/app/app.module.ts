import * as Raven from 'raven-js';  //Error reporting
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatSidenavModule, MatToolbarModule, MatListModule, MatIconModule,
  MatButtonModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CustomerModule } from './customer/customer.module';
import { OrderModule } from './order/order.module';
import { ArticleService } from './article.service';
import { LogInModule } from './login/login.module';
import { routing } from './app.routes';
import { LoggedInGuard } from './login/logged-in.guard';
import { ToastrModule } from 'ngx-toastr';
import {Component} from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import { VERSION } from 'environments/version';
import { environment } from 'environments/environment';

//Error reporting - sentry.io
Raven
  .config(environment.sentryEndpoint, {
    release: VERSION.version,
    tags: {git_commit: VERSION.hash},
    environment: environment.production ? 'production' : 'non-production'
  })
  .install();

export class RavenErrorHandler implements ErrorHandler {
  handleError(err: any): void {
    Raven.captureException(err);
    console.log(err);
  }
}



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    CustomerModule,
    OrderModule,
    LogInModule,
    HttpModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    ArticleService,
    LoggedInGuard, 
    { provide: ErrorHandler, useClass: RavenErrorHandler },
    { provide: MAT_DATE_LOCALE, useValue: 'it-IT'}
  ],
  exports: [HttpModule],
  bootstrap: [AppComponent]
})
export class AppModule { }