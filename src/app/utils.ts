export function genId() {
  // generates unique random string
  return new Date().getTime() + Math.random().toString(36).substr(9);
}