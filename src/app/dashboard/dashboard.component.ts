import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx'
import PouchDB from 'pouchdb';
import PouchdbFind from 'pouchdb-find';
import { OrderListComponent } from '../order/order-list/order-list.component';
import { environment } from 'environments/environment';
import { CustomerService } from 'app/customer/customer.service';
import { OrderService } from 'app/order/order.service';
import { ToastrService } from 'ngx-toastr';
import { VERSION } from './../../environments/version';


@Component({
  selector: 'dashboard',
  templateUrl: `./dashboard.component.html`,
  styles: [],
  providers: [
    OrderListComponent
  ]
})

export class DashboardComponent implements OnInit {
  public git_hash = VERSION.hash;
  public onlineFlagObservable: Observable<boolean>;
  public onlineFlag: boolean;
  public onlineFlagLabel: string;

  public ordersDB: any;
  public customersDB: any;
  public catalogDB: any;

  public ordersLocalCount: number;
  public customersLocalCount: number;


  private success: boolean = true;
  public _remoteDB: any;
  public _syncOpts: any;

  public syncCustomersDate: any; 
  // public syncStatus: any; 
  public totRead: any; 
  public totWrite: any;
  public customersLocalBuffer: PouchDB;
  public customersToUpload = [];
  public customersToUploadCount: number;

  public syncOrderDate: any;
  public syncOrderStatus: any = "NON AVVIATO";
  public totOrderRead: any; 
  public totOrderWrite: any;

  public ordersLocalBuffer: PouchDB;
  public ordersToUpload = [];
  public ordersToUploadCount: number;

  public syncCatalogDate: Date;
  public syncCatalogStatus: any = "NON AVVIATO";

  public syncCustomersStatus: any = "NON AVVIATO";


  public customerTable:any;
  public orderTable:any;
  private remoteCustomerDb:any;
  private remoteOrderDb:any;
  private remoteCatalogDb:any;

  private apiurl = environment.apiEndPoint;
  private remoteApiurl = environment.remoteCouchDB;

  filtertotalItems: any = { count: 0 };
  customersToSync = [];

  ordersToSync = [];
  orders = [];
  ordersOnline = [];

  private totOrders: any = 0;
  private totCustomers: any = 0;

  constructor(private http: Http, private customerService: CustomerService, private orderService: OrderService, private toastr: ToastrService) {

    this.customerTable=localStorage.getItem('customer_table');
    this.orderTable=localStorage.getItem('order_table');  
    

    //Update sync label if something is stored for offline use
    if(this.customerTable.length > 0 ) this.syncCustomersStatus = "OK (offline)";
    if(this.orderTable.length > 0 )    this.syncOrderStatus     = "OK (offline)";

//
  //  syncCatalogStatus


    this.remoteCustomerDb = new PouchDB(this.remoteApiurl + this.customerTable, {
      auto_compaction: true, auth: {
        username: environment.dbUserName,
        password: environment.dbUserPassword
      }
    });
    
    this.remoteOrderDb = new PouchDB(this.remoteApiurl + this.orderTable, {
      auto_compaction: true, auth: {
        username: environment.dbUserName,
        password: environment.dbUserPassword
      }
    });

    this.remoteCatalogDb = new PouchDB(this.remoteApiurl + 'catalog', {
      auto_compaction: true, auth: {
        username: environment.dbUserName,
        password: environment.dbUserPassword
      }
    });

    //Buffer (customers / orders waiting to be uploaded)
    this.customersLocalBuffer = new PouchDB('customersLocalBuffer');
    this.ordersLocalBuffer = new PouchDB('ordersLocalBuffer');


    //CONNECTION CHECK **************************

    //Connection observable
    this.onlineFlagObservable = Observable.merge(
      Observable.of(navigator.onLine),
      Observable.fromEvent(window, 'online').mapTo(true),
      Observable.fromEvent(window, 'offline').mapTo(false)
      );


    //WATCH CONNECTION CHANGE
    this.onlineFlagObservable.subscribe( change => {
      this.onlineFlag = change;

      //Update label and perform sync tasks
      if(change){
        this.onlineFlagLabel = "online";
        //this.startSync();  //TODO: uncomment to enamble auto-sync when online
      } 
      else {
        this.onlineFlagLabel = "offline";
      }

    });

  }


  ngOnInit() {

    this.totOrders = this.ordersToSync.length;
    this.totCustomers = this.customersToSync.length;

    this.startSync()

  }


  startSync(){
        this.pushBufferedCustomerToServer();
        this.ReplicateCustomerDB();

        this.pushBufferedOrderToServer();
        this.ReplicateOrderDB();

        debugger
        this.ReplicateCatalogDB();  
  }


  ReplicateCustomerDB() {
    debugger
    this.customersDB = new PouchDB(this.customerTable);
    this._remoteDB = this.remoteCustomerDb;
    this._syncOpts = {
      live: false,
      retry: true,
      continuous: false
    };

    this.syncCustomersStatus = "IN CORSO";
    this.customersDB.replicate.from(this._remoteDB,this._syncOpts)
    .on('change', (change) => {
      debugger
     
      if(change.ok==true){
        this.syncCustomersStatus = "OK";
        this.syncCustomersDate = change.start_time;
        this.countLocalItem(); 
      }
      else{
        this.syncCustomersStatus = "IN CORSO...";
      }
     
      // handle change
    }).on('complete', (complete) => {
      this.syncCustomersStatus = "OK";
      this.syncCustomersDate = complete.start_time;
      this.countLocalItem(); 
    }).on('error', function (err) {
      this.syncCustomersStatus = "ERRORE";
      console.log(err);
    });


  }

  ReplicateOrderDB() {
    debugger
    let PdbWithFind = PouchDB.plugin(PouchdbFind);
    this.ordersDB = new PdbWithFind(this.orderTable);
    this._remoteDB = this.remoteOrderDb;
    this._syncOpts = {
      live: false,
      retry: true,
      continuous: false
    };
debugger
    this.syncOrderStatus = "IN CORSO";

    this.ordersDB.replicate.from(this._remoteDB,this._syncOpts)
    .on('change', (change) => {
      debugger

      if(change.ok==true){
        localStorage.setItem('syncOrderStatus',"OK");
        this.syncOrderStatus = "OK";
        this.syncOrderDate = change.start_time;
        this.countLocalItem(); 
      }
      else{
        localStorage.setItem('syncOrderStatus',"IN CORSO");
        this.syncOrderStatus = "IN CORSO...";
      }

        // handle change
      }).on('complete', (complete) => {
        this.syncOrderStatus = "OK";
        this.syncOrderDate = complete.start_time;
        this.countLocalItem(); 
      }).on('error', function (err) {
        this.syncOrderStatus = "ERRORE";
        console.log(err);
      });      

    }


  ReplicateCatalogDB() {
    let PdbWithFind = PouchDB.plugin(PouchdbFind);
    this.catalogDB = new PdbWithFind('catalog');
    this._remoteDB = this.remoteCatalogDb;
    this._syncOpts = {
      live: false,
      retry: true,
      continuous: false
    };
    
    this.syncCatalogStatus = "IN CORSO";

    this.catalogDB.replicate.from(this._remoteDB,this._syncOpts)
    .on('change', (change) => {
      debugger
      localStorage.setItem('syncCatalogDate',change.start_time);

      if(change.ok==true){
        this.syncCatalogStatus = "OK";
        this.syncCatalogDate = change.start_time;
        this.countLocalItem(); 
      }
      else{
        this.syncCatalogStatus = "IN CORSO...";
      }
    }).on('complete', (complete) => {
      debugger
      this.syncCatalogStatus = "OK";
      this.syncCatalogDate = complete.start_time;

    }).on('error', (err) => {
      this.syncCatalogStatus = "Errore nella sincronizzazione";
      console.log(err);
    });     
  }



  //response in the 'change' listener:
  // {
  //   "doc_write_failures": 0,
  //   "docs_read": 1,
  //   "docs_written": 1,
  //   "errors": [],
  //   "last_seq": 1,
  //   "ok": true,
  //   "start_time": "Fri May 16 2014 18:23:12 GMT-0700 (PDT)",
  //   "docs": [
  //     { _id: 'docId',
  //       _rev: '1-e798a1a7eb4247b399dbfec84ca699d4',
  //       and: 'data' }
  //   ]
  // }

  // response in the 'complete' listener:
  // {
  //   "doc_write_failures": 0,
  //   "docs_read": 2,
  //   "docs_written": 2,
  //   "end_time": "Fri May 16 2014 18:26:00 GMT-0700 (PDT)",
  //   "errors": [],
  //   "last_seq": 2,
  //   "ok": true,
  //   "start_time": "Fri May 16 2014 18:26:00 GMT-0700 (PDT)",
  //   "status": "complete"
  // }

  pushBufferedCustomerToServer() {
    this.customerService.fetchBuffer().then(result => {
      console.log('Customers buffer: ' + result);
      debugger
      this.customersToUpload = result.rows.slice();
      this.customersToUploadCount = this.customersToUpload.length;

      //If online, push to the server
      if(window.navigator.onLine){
        this.customersToUpload.forEach(cus => {
          this.customerService.createNewCustomer(cus.doc).subscribe(
            result => {
              debugger
              //delete from the buffer once uploaded
              this.customerService.deleteCustomer(cus.doc._id, this.customersLocalBuffer).then(() => {
                  this.customersToUploadCount -= 1;           
              });
            }, error => { 
              this.toastr.error('Si è verificato un errore durante l\'invio al server dei nuovi clienti salvati. I dati resteranno salvati su questo dispositivo.', 'Impossibile salvare i nuovi clienti');
            });
        });
      }
    });
  }




  // updateLocalOrdersToServer() {
  //   this.orderService.fetchAllOrders()
  //     .then(response => {
  //       this.orders = response.rows.slice();
  //       if (this.orders[0].id.includes('_design')) {
  //         this.orders.splice(0, 1);
  //         console.log(this.orders);
  //       }
  //       this.orders.forEach(element => {
  //         if (element.doc.customer_id !== undefined && element.doc.customer_id !== '') {
  //           this.customerService.fetchCustomer(element.doc.customer_id).then(result => {
  //             console.log(result);
  //             element.doc.customerName = result.name;
  //           })
  //             .catch(error => console.log(error));
  //         }
  //       });
  //       console.log(this.orders);
  //       this.updateOrders();
  //     })
  //     .catch(error => console.log('ERROR WHILD FETCHING ORDERS: ', error));

  // }

    pushBufferedOrderToServer() {
    this.orderService.fetchBuffer().then(result => {
      console.log('Customers buffer: ' + result);
      debugger
      this.ordersToUpload = result.rows.slice();
      this.ordersToUploadCount = this.ordersToUpload.length;

      //If online, push to the server
      if(window.navigator.onLine){
        this.ordersToUpload.forEach(order => {
          this.orderService.saveOrderToServer(order.doc).subscribe(
            result => {
              debugger
              //delete from the buffer once uploaded
              this.orderService.deleteOrder(order.doc._id, this.ordersLocalBuffer).then(() => {
                  this.ordersToUploadCount -= 1;           
              });
            }, error => { 
              this.toastr.error('Si è verificato un errore durante l\'invio al server dei nuovi ordini salvati. I dati resteranno salvati su questo dispositivo.', 'Impossibile salvare i nuovi ordini');
            });
        });
      }
      else{ console.log('Offline: can\'t push new ORDERS'); }
    });
  }

  // updateOrders() {
  //   this.orders.forEach(order => {
  //     const array = Array<any>();
  //     if (order.doc.addedOffline) {
  //       for (const elem in order.doc.articles) {
  //         if (order.doc.articles.hasOwnProperty(elem)) {
  //           order.doc.articles[elem].articles.forEach(ar => {
  //             const arObj = { 'artCode': ar.code, 'artDesc': ar.code, 'qtyOrdered': ar.qty_requested, 'unitPrice': ar.price };
  //             array.push(arObj);
  //           });
  //         }
  //       }
  //       order.doc.articles = array;
  //       this.ordersToSync.push(order.doc);
  //     }
  //   });
  //   this.totOrders = this.ordersToSync.length;
  //   this.addOrdersToServer();
  // }

  // addOrdersToServer() {
  //   this.ordersToSync.forEach(order => {

  //     this.orderService.saveOrderToServer(order).subscribe(result => {
  //       console.log(result);
  //   this.orderService.deleteOrder(order.id)
  //     }, error => { 
  //       this.toastr.error('Si è verificato un errore durante l\'invio al server degli ordini salvati. I dati resteranno salvati su questo dispositivo.', 'Impossibile inviare gli ordini');
  //       console.log(error); 
  //     });
  //   });
  // }

  /**
    * Count items that are stored for offline use
    */
  countLocalItem(){
    this.ordersDB.info().then( result => {
      this.ordersLocalCount = result.doc_count;
    }).catch(function (err) {
      console.log(err);
    });

    this.customersDB.info().then( result => {
      this.customersLocalCount = result.doc_count;
    }).catch(function (err) {
      console.log(err);
    });

  }


}
