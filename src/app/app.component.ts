import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import PouchDB from 'pouchdb';
import { environment } from 'environments/environment';
import { VERSION } from 'environments/version';

// import { OrderService } from './order/order.service';
// import { CustomerService } from './customer/customer.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  // sidenav = {
  //   opened: false,
  //   mode: 'side',
  //   toggle_button: true,
  //   autoclose: false
  // };
  sidenav_mode = 'side';
  opened = true;

  show_staging_banner: boolean = false;
  environment_message: string;

  git_hash = VERSION.hash;

  constructor(private router: Router,
              // private orderService: OrderService,
              //private customerService: CustomerService,
              ) {

    if(environment.message != 'production')  this.show_staging_banner = true;
    this.environment_message = environment.message;
  }

  ngOnInit() {
    if (window.innerWidth < 980) {
      // this.sidenav.opened = false;
      this.sidenav_mode = 'over';
      // this.sidenav.toggle_button = true;
      // this.sidenav.autoclose = true;
      this.opened = false;
    }

  }


  logOut() {

    //Unregister ServiceWorker
    navigator.serviceWorker.getRegistrations().then(function(registrations) {
     for(let registration of registrations) {
      registration.unregister();
      console.log('ServiceWorker unregistred');
    } });

    //Delete databases
    let tableNamesToInspect = [localStorage.getItem('customer_table'), 
                               localStorage.getItem('order_table'),
                               'catalog',
                               'ordersLocalBuffer',
                               'customersLocalBuffer'
                               ];
    tableNamesToInspect.forEach(function(tableName) {
      if(tableName){
          try{
            console.log('PouchDB destroy: ' + tableName);
            let db = new PouchDB(tableName);
            db.destroy();
          } catch(err){
            console.error('Cannot destroy: ' + err);
          }
      }
    });

    localStorage.clear();
    //this.router.navigate(['login']);  //Generate an error if a ned login is performed without refreshing the page
    
    //Force page
    alert('Logout effettuato');
    window.location.href = 'https://www.google.com';
  }

  isLoggedIn() {
    const token = localStorage.getItem('login_status');
    if (token != null) {
      return true;
    }
    return false;
  }

}
