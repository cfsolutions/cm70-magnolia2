import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs/Observable';
import PouchDB from 'pouchdb';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Customer } from './customer';
import { genId } from '../utils';

@Injectable()
export class CustomerService {
  orderTable: string;
  customerTable: string;
  private db: PouchDB;
  private customersLocalBufferDB: PouchDB;
  private apiurl = environment.apiEndPoint
  private remoteApiurl = environment.remoteCouchDB;  
  name:any;

  constructor(private http: Http) {

    this.customerTable=localStorage.getItem('customer_table');
    this.orderTable=localStorage.getItem('order_table');

    this.db = new PouchDB(this.customerTable);    
    this.customersLocalBufferDB = new PouchDB('customersLocalBuffer');
    this.remoteApiurl = environment.remoteCouchDB + this.customerTable;
    this.apiurl=environment.apiEndPoint + "customer";
    this.name=this.customerTable;
      
  }

  /**
    * Store customer on the local device 
    */
  storeOnBuffer(customer): Promise<any> {
    // customer.customerSince = new Date().toISOString();
    customer.customerId = genId();
    customer.addedOffline = true;
    customer.agentCode = localStorage.getItem('agentCode')
    console.log(customer);
    return this.customersLocalBufferDB.post(customer);
  }


  fetchBuffer(): Promise<any> {
    return this.customersLocalBufferDB.allDocs({
      include_docs: true,
      sort: ['name']
    });
  }


  fetchAll(): Promise<any> {
    return this.db.allDocs({
      include_docs: true,
      sort: ['name']
    });
  }

  fetchCustomer(id): Promise<any> {
    return this.db.get(id);
  }


  getCustomerList(): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.apiurl, { headers: headers }).map((response: Response) => <any>response)
      .catch(this.handleError)
  }

  createNewCustomer(obj: Customer): Observable<Response> {
    var username = localStorage.getItem('username');
    var password = localStorage.getItem('password');

    const headers = new Headers();
    headers.append("Authorization", "Basic " + btoa(username + ":" + password)); 
    debugger
    return this.http.post(this.apiurl, obj, { headers: headers }).map((response: Response) => { 
      console.log('New customer response status: ' + response.status); 
      return response;
    })
    .catch(this.handleError)
  }

  getCustomerSearch(obj: any): Observable<any> {
    var username = localStorage.getItem('username');
    var password = localStorage.getItem('password');

    const headers = new Headers();
    headers.append("Authorization", "Basic " + btoa(username + ":" + password)); 

    return this.http.post(this.apiurl + '/search/',  obj, { headers: headers } ).map((response: Response) => <any>response)
      .catch(this.handleError)

  }

  getCustomerOrderStatistics(customerId: any): Observable<any> {
    var username = localStorage.getItem('username');
    var password = localStorage.getItem('password');

    const headers = new Headers();
    headers.append("Authorization", "Basic " + btoa(username + ":" + password)); 

    return this.http.get(this.apiurl + '/' +customerId + '/statistics',  { headers: headers }).map((response: Response) => <any>response)
      .catch(this.handleError)
  }

  fetchCustomerOrders(id): Promise<any> {
    return this.db.find({ selector: { customer_id: id } });
  }

  // deleteCustomer(id:any, pouchDB:PouchDB) {
  //   pouchDB.get(id).then(function(doc) {
  //     return pouchDB.remove(doc);
  //   }).then(function (result) {
  //     pouchDB.compact();
  //   }).catch(function (err) {
  //     console.log(err);
  //   });
  // }
  
   deleteCustomer(id:any, pouchDB:PouchDB) {
    
    return new Promise(
      function (resolve, reject) {
        pouchDB.get(id).then(function(doc) {
          return pouchDB.remove(doc);
        }).then(function (result) {
          pouchDB.compact();
          resolve();
        }).catch(function (err) {
          console.log(err);
          reject(err);
        });
    })
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;

      //If the device is offline
      if(error.status == 0){

      }

    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
