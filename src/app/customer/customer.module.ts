import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MaterialInputModule } from '../material-input.module';

import { CustomerNewComponent } from './customer-new/customer-new.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CustomerDetailComponent } from './customer-detail/customer-detail.component';
import { CustomerService } from './customer.service';
import { LoggedInGuard } from '../login/logged-in.guard';
import { CustomerFilterPipe } from './customer-list/customer.filter.pipe';


const moduleRoutes: Routes = [
  {
    path: 'customer', children: [
      { path: 'new', component: CustomerNewComponent, canActivate: [LoggedInGuard] },
      { path: 'list', component: CustomerListComponent, canActivate: [LoggedInGuard] },
      { path: 'detail/:customerId', component: CustomerDetailComponent, canActivate: [LoggedInGuard] },
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(moduleRoutes),
    MaterialInputModule,
  ],
  declarations: [
    CustomerNewComponent,
    CustomerListComponent,
    CustomerDetailComponent,
    CustomerFilterPipe
  ],
  providers: [CustomerService, LoggedInGuard]
})
export class CustomerModule { }
