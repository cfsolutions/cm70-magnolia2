import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'customerSearch'

})
export class CustomerFilterPipe implements PipeTransform {

    transform(value: any[], args: any[]): any[] {
debugger
        if (args == null || args.length === 0 || value === undefined) { return value; }
        const customerNameFilter: string = args[0] ? args[0].toLowerCase() : null;
        const brandFilter:        string = args[1] ? args[1].toLowerCase() : null;
        const count = args[2];
        const filterArray = [];

        if (customerNameFilter == null && brandFilter == null) {
            count.count = value.length;
            return value
        }

        value.forEach(function (elem) {
            for (const property in elem.doc) {
                if (elem.doc[property] && typeof elem.doc[property] === 'string' && 
                    elem.doc[property].toLocaleLowerCase().startsWith(customerNameFilter)) {
                    filterArray.push(elem);
                    break;
                } else if (property == 'orderedBrands') {
                    for (const arr in elem.doc[property]) {
                        if (typeof arr === 'string' && elem.doc[property][arr].toLocaleLowerCase().startsWith(brandFilter)) {
                            filterArray.push(elem)
                            break;
                        }
                    }

                }

            }
        });

        return filterArray;

    }
}