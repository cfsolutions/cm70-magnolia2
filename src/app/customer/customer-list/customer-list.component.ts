import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CustomerService } from '../customer.service';
import { OrderService } from '../../order/order.service';

import { ToastrService } from 'ngx-toastr';

// import '../../order/order';
import { BrandList } from '../../order/order';


@Component({
  selector: 'customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css'],
})

export class CustomerListComponent implements OnInit {
  customers = [];
  filtertotalItems: any = { count: 0 };
  searchText: '';
  searchBrand: ''
  online: boolean = false;

  brandList: BrandList;
  brands = { all: [], prev: [], new: [] };

  
  // catalog = [];
  // clean_catalog = [];
  // brands = { all: [], prev: [], new: [] };

  constructor(private customerService: CustomerService, 
              private orderService: OrderService,
              private router: Router,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.fetchCustomers();
    // this.fetchArticles();
  }

  fetchCustomers() {
    this.customerService.fetchAll().then(result => {
      debugger
      console.log("customer:" + result);
      this.customers = [];
      this.customers = result.rows.slice();
      this.filtertotalItems.count = this.customers.length;
    });
  }

  getCustomerSearchByBrand(event: any, option: string) {
    if (event.source.selected) {
      if (option === 'All') {
        this.fetchCustomers();
        return;
      } else {
        const obj = { 'brand': option };
        this.customerService.getCustomerSearch(obj).subscribe(
          result => {
            if (result.status === 200) {
              console.log(result);
              const list = JSON.parse(result._body).customers;
              this.filtertotalItems.count = list.length;
              this.customers = [];
              list.forEach(element => {
                const obj = { 'doc': element };
                this.customers.push(obj);
              });
            }
          },
          error => {
            this.toastr.error('Impossibile effettuare la ricerca.');
          }
        );
      }
    }
  }

  customerDetails(customerId: string) {
    this.router.navigate(['customer/detail/', customerId]);
  }

  fetchArticles() {
    this.orderService.fetchArticles()
      .then(response => {
        debugger
        let catalog = response.rows[0].doc.catalog;
        let brandCodes = Object.keys(catalog).sort();
        
        brandCodes.forEach(brandCode => {
        this.brands.all.push({"brandCode": brandCode,
                                "brandName": catalog[brandCode].brandName});
        });

        this.brandList = new BrandList(this.brands.all);
        console.log(this.brandList);
      })
      .catch(error => console.log('ERROR WHILE FETCHING ARTICLES', error)); 
  }


}

