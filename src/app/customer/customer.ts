export class Customer {
  customerId: string;
  name: string;
  shopName: string;
  address: string;
  city: string;
  telephone: string;
  email: string;
  VAT: string;
  customerSince: Date;
  province: string;
  zipCode: string;
  url: string;
  IBAN: string;
  shopType: string;
  pendingArticles: any[];
  orderedBrands: any[];
  contactName: string;
  notes: string;
  agentCode: string;

  userId: string;
  addedOffline: boolean;

  constructor(
  //   public _id: string = '',
  //   public customer_since: string = '',
  //   public customer_name: string,
  //   public customer_address: string,
  //   public customer_city: string,
  //   public phone: string,
  //   public customer_email: string,
  //   public cf: string,
  //   public customer_vat: string,
  //   public prev_articles: {},
  //   {
  //     brand1: [
  //       article1,
  //       article2,
  //     ],
  //     brand2: [
  //       article3,
  //       article4,
  //     ]
  //   }
   ) { }

}
