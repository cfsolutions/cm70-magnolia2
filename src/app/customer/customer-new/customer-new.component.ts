import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Customer } from '../customer';
import { CustomerService } from '../customer.service';

import { ToastrService } from 'ngx-toastr';


@Component({
  templateUrl: './customer-new.component.html',
  styleUrls: ['./customer-new.component.css']
})
export class CustomerNewComponent implements OnInit {
  // form:any = {};
  form: Customer = new Customer();
  shopTypes: any;

  constructor(
    private customerService: CustomerService,
    private router: Router,
    private toastr: ToastrService
  ) {   }

  ngOnInit() {
    this.form.customerSince = new Date();
    this.shopTypes = ['Profumeria',
                      'Concept Store',
                      'Farmacia',
                      'Fashion Store',
                      'Erboristeria',
                      'Altro'];
  }

  saveCustomer() {
    this.form.agentCode = localStorage.getItem('agentCode')

    let deviceOnline = window.navigator.onLine;

    //If online, store on the remote server
    if(deviceOnline === true){
      this.customerService.createNewCustomer(this.form).subscribe(
        result => {
          if(result.status == 201)  this.toastr.success('I dati del nuovo cliente sono stati salvati sul server.', 'Salvato');
          else                      console.log('RESULT STATUS NOT EXPECTED');

          this.router.navigate(['/customer/list']);
          
        },
        error => { 
          this.toastr.error('Errore durante la connessione con il server.', 'Impossibile i dati');
          console.log(error); 
        })
    }
    //Else store locally
    else{
      this.toastr.warning('I dati saranno salvati su questo dispositivo in attesa che la connessione internet venga ristabilita.', 'Dispositivo offline');

      this.customerService.storeOnBuffer(this.form)
        .then(() => {
          this.router.navigate(['/customer/list']);
        })
        .catch(error => {
          console.log('ERROR WHILE SAVING CUSTOMER: ', error)
          this.toastr.error('Impossibile salvare i dati nel dispositivo.', 'ERRORE!');
        });
    }
  }

}
