import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CustomerService } from '../customer.service';
import { Customer } from '../customer';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css'],
})
export class CustomerDetailComponent implements OnInit {

  customerId = 0;
  customerObj = new Customer();
  customer: any;
  statisticsObj: any;
  years: any[];

  constructor(private customerService: CustomerService, private router: Router, private _activateRoute: ActivatedRoute) { }

  ngOnInit() {
    // this.customerObj = {
    //   customerId: '', address: '', city: '', customerSince: new Date(), email: '',
    //   iban: '', name: '', orderedBrands: [], pendingArticles: [], phone: '', province: '',
    //   shopName: '', shopType: '', url: '', vat: '', zipCode: '',userId: '',
    //   addedOffline: true
    // };
    this.customerObj = new Customer;
    this.customerObj.addedOffline = true;

    this.customer = { doc: this.customerObj };
    this.statisticsObj = { statistics : [], error: false, emptyResponse: null};
    this.years = [];

    console.log(this.customer);

    this._activateRoute.params.subscribe(
      (param: any) => {
        this.customerId = param['customerId'];
      });
    
     this.fetchCustomer(); 
  }


  fetchCustomer() {
    this.customerService.fetchCustomer(this.customerId).then(result => {
      this.customer.doc = result;
      this.getCustomerStatistics();

    })
      .catch(error => console.log(error));
  }

  getCustomerStatistics() {
    this.customerService.getCustomerOrderStatistics(this.customer.doc.customerCode).subscribe(result => {
      debugger
      if (result.status === 200) {
        this.statisticsObj = JSON.parse(result._body);
        if(this.statisticsObj.statistics.legth == 0){
          this.statisticsObj.emptyResponse = true;
        }
      }
    });
  }

}
