import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import 'rxjs/add/operator/switchMap';
// import 'rxjs/add/observable/toPromise';

import { OrderService } from '../order.service';
import { CustomerService } from '../../customer/customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'order-detail',
  templateUrl: './order-detail.component.html',
  styles: []
})
export class OrderDetailComponent implements OnInit {
  order: any;
  brands: string[];
  loading: boolean = true;

  constructor(
    private orderService: OrderService,
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {      

    this.route.paramMap.subscribe(params => {
        let database = params.get('database');  //Database (offline: local PouchDB / Online)

        if(database == 'offline')      this.getOfflineOrder(params.get('id'));
        else if(database == 'online')  this.getOnlineOrder(params.get('id'));
        else     console.error('WRONG DATABASE PARAMETER (can be ONLINE or OFFLINE)');
    });
  }

  getOfflineOrder(riferimentoMagnolia){
    this.orderService.fetchOrder(riferimentoMagnolia).then(order => {
        debugger;
        this.loading = false;
        this.order = order;
        if (this.order.customer_id !== undefined && this.order.customer_id !== '') {
          this.customerService.fetchCustomer(this.order.customer_id).then(result => {
            console.log(result);
            this.order.customerName = result.name;
          })
            .catch(error => console.log(error));
        }
        this.brands = Object.keys(order.articles);
        console.log(this.order);
        console.log(this.brands);
      });
  }

  getOnlineOrder(riferimentoMagnolia){
    let result = this.orderService.fetchOnlineOrder(riferimentoMagnolia).map((response: Response) => <any>response).subscribe(
      result => {
        this.loading = false;

        if (result.status === 200) {
          debugger
          this.order =  JSON.parse(result._body);
          this.brands = Object.keys(this.order.articles);
        }
      },
      error => {
        this.toastr.error('Impossibile ottenre i dati dal server.');
        console.error(error);
      });
  }


}
