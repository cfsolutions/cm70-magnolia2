import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, ReactiveFormsModule} from '@angular/forms';
import {MatTabsModule} from '@angular/material/tabs';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import { ToastrService } from 'ngx-toastr';

import { CustomerService } from '../../customer/customer.service';
import { OrderService } from '../order.service';

import { BrandList } from '../order';

import * as _ from 'lodash';


// class CustomersList{
//   public customersArray: Customer[];


//   constructor(customerList){
//     this.customersArray = [];
//
//     customerList.forEach(customerRow => {
//       let customer = new Customer(customerRow);
//       this.customersArray.push(customer);
//     })
//   }
// }

// class Customer{
//   public name: string;
//   public couchID: string;
//   public customerCode: string;

//   constructor(customerRow){
//     this.name = customerRow.doc.name;
//     this.couchID = customerRow.doc._id;
//     this.customerCode = customerRow.doc.customerCode;
//   }

// }

class Product {
  category: string = 'onSell';
  code: string = '';
  desc: string = '';
  price: number = 0;
  class: string = '';
  qty_requested: number = 0;
  qty_pending: number = 0;
}


class ArticleList {
  prev: object;
  new: object;
  all: object;


  constructor() {
    this.prev = { onSell: [], pos: [], outOfStock: [] };
    this.new = { onSell: [], pos: [], outOfStock: [] };
    this.all = { onSell: [], pos: [], outOfStock: [] };
  }

  /**
   * Return both prev+new array AND sort
   */
  updateAndSort(){
    this.all = { onSell: [], pos: [], outOfStock: [] };

    //Merge
    this.all['onSell']     = this.prev['onSell'].concat(this.new['onSell']);
    this.all['pos']        = this.prev['pos'].concat(this.new['pos']);
    this.all['outOfStock'] = this.prev['outOfStock'].concat(this.new['outOfStock']);

    //Sort
    // this.all['onSell'].sort(function(a, b) {debugger
    //   return a['ar_linea'] < b['ar_linea']  ||  a['ar_ordsta'] < b['ar_ordsta'];
    // });
    // this.all['pos'].sort(function(a, b) {
    //   return a['ar_linea'] < b['ar_linea']  ||  a['ar_ordsta'] < b['ar_ordsta'];
    // });
    // this.all['outOfStock'].sort(function(a, b) {
    //   return a['ar_linea'] < b['ar_linea']  ||  a['ar_ordsta'] < b['ar_ordsta'];
    // });
    
    // return this.all;
    
    this.all['onSell']     = _.sortBy(this.all['onSell'], ['ar_linea', 'ar_ordsta']);
    this.all['pos']        = _.sortBy(this.all['pos'], ['ar_linea', 'ar_ordsta']);
    this.all['outOfStock'] = _.sortBy(this.all['outOfStock'], ['ar_linea', 'ar_ordsta']);
  }

}

@Component({
  selector: 'order-new',
  templateUrl: './order-new.component.html',
  styleUrls: ['./order-new.component.css'],

})


export class OrderNewComponent implements OnInit {

  brandAutocomplete: FormControl = new FormControl();
  brandAutocompleteFilteredOptions: Observable<string[]>;

  brandList: BrandList;
  order = {
    customer_couchId: '',
    customerCode: '',
    customerName: '',
    articles: {},
    pendingArticlesToDelete: {},
    upload_date: new Date(),
    total: 0,
    totalQty: 0,
    stats: {
      onSell_amount: 0,
      onSell_qty: 0,
      pos_amount: 0,
      pos_qty: 0
    },
    shippingDatePreferred: new Date(),
    shippingAddress: '',
    paymentType: '',
    note: '',
  };
  customers = [];
  catalog = [];
  // clean_catalog = [];
  brands = { all: [], prev: [], new: [] };
  selected_brand: {
    code: string,
    onSell_amount: any,
    pos_amount: any
  };

  selected_customer: { 'prev_articles', 'shippingAddresses' };
  selected_articles = {};
  selected_articles_summary = [];  //Showed to the user before confirming the order
  article_categories = [ {'code': 'onSell', 'title': 'In vendita'}, 
                         {'code': 'pos',    'title': 'POS'} ];
  article_list: ArticleList;
  articles_for_confirmation = {};
  pending_articles: any[];
  pending_articles_for_confirmation: any[];
  confirmed_articles = {};  //TODO: variable no longer needed (if a pending article is selected, it must be deleted from the previous orders)
  pending_articles_to_delete = {};
  filter_key = '';
  prev_articles: any = {};  // previously ordered articles
  // available states: 0 (genferal form), 1(confirm previous),
  //                   2 (choose brand), 3 (choose product)
  state = 0;
  state_titles = [
    'Nuovo ordine', 'Conferma', 'Elenco brand', 'Prodotti brand'
  ];
  objectKeys = Object.keys;
  filterArticlesRx = new Subject();
  date = new Date();

  selectedCustomerId: any;

  // customersList: CustomersList;
  customerAutocomplete = new FormControl();
  customersAutocompleteFilteredOptions: Observable<string[]>;

  constructor(
    private customerService: CustomerService,
    private orderService: OrderService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this.filterArticlesRx.debounceTime(500).distinctUntilChanged().subscribe(
      (key: string) => this.filterArticles(key)
    );
  }

  ngOnInit() {

    this.fetchCustomers();
    this.fetchArticles();

    this.selected_brand = {
      'code': '',
      'onSell_amount': 0,
      'pos_amount': 0
    };

    this.selected_customer = { 'prev_articles': [], 'shippingAddresses': [] };

  }

  ///// Customer Autocomplete
  setAutocomplete() {
    this.customersAutocompleteFilteredOptions = this.customerAutocomplete.valueChanges
      .pipe(
        startWith(''),
        map(val => val ? this.customersAutocompleteFilter(val) : this.customers.slice())
      );

  }

  customersAutocompleteFilter(val): string[] {
    console.log(val);
    return this.customers.filter(option => {
      if (val !== undefined && typeof val === 'string') {
          return option.doc.name.toLowerCase().startsWith(val.toLowerCase());
      }
    });
  }

  autocompleteDisplayCustomerName(customer?): string | undefined {
    return customer ? customer.doc.name : undefined;
  }

  ///////////////////////////////////


  nextState(state = null) {
    if (state !== null ) {
      this.state = state;

    } else {
      if (this.state === 0 && Object.keys(this.articles_for_confirmation).length == 0) {
        // skip confirmation if nothing to confirm
        this.state += 2;
      } else {
        this.state++;
      }
    }

    if (state == 4) {
      this.showCartSummary();
    }
    window.scroll(0, 0);
  }

  previousState() {
    if (this.state === 2 && Object.keys(this.articles_for_confirmation).length == 0) {
      // skip confirmation if nothing to confirm
      this.state -= 2;

    }else if (this.state === 4) {
      //if on the summary page, go to the brand list (not the articles list)
      this.state -= 2;
    } else {
      this.state--;
    }

    //Force data reset if the user comes back to the main state
    if (this.state==0) {
      this.router.navigate(['/dashboard']);
    }
    window.scroll(0, 0);
  }
  resetState() {
    this.state = 0;
    window.scroll(0, 0);
    this.filter_key = '';
  }

  fetchCustomers() {
    this.customerService.fetchAll()
      .then(response => {
        this.customers = response.rows;
        // this.customersList = new CustomersList(this.customers);
        this.setAutocomplete();
      })
      .catch(error => console.log('ERROR WHILE FETCHING CUSTOMERS: ', error));
  }

  fetchArticles() {
    this.orderService.fetchArticles()
      .then(response => {
        this.catalog = response.rows[0].doc.catalog;
        const brandCodes = Object.keys(this.catalog).sort();

        // Force conversion from string to boolean
        // ( https://stackoverflow.com/questions/3263161/cannot-set-boolean-values-in-localstorage )
        const user_isShop = JSON.parse(localStorage.getItem('isShop'));

        //Prepare brand list and filter brands for shop only
        brandCodes.forEach(brandCode => {
          if (user_isShop || !(this.catalog[brandCode].onlyStores)) {
            this.brands.all.push({'brandCode': brandCode,
                                  'brandName': this.catalog[brandCode].brandName});
          }
        });

        this.brandList = new BrandList(this.brands.all);
        console.log(this.catalog);
        console.log(this.brandList);
      })
      .catch(error => console.log('ERROR WHILE FETCHING ARTICLES', error));
  }

  setArticleList() {
    this.article_list = new ArticleList();
    this.article_categories.forEach(cat => {
      if (this.catalog[this.selected_brand.code].hasOwnProperty(cat.code)) {
        this.catalog[this.selected_brand.code][cat.code].forEach(article => {
          if (this.confirmed_articles[article.code]) {
            article.qty_requested = this.confirmed_articles[article.code];
            delete this.confirmed_articles[article.code];
          }

          if ((this.prev_articles || []).indexOf(article.code) != -1) {
            this.article_list.prev[cat.code].push(article);
          } else {
            this.article_list.new[cat.code].push(article);
          }
        });
      }
    });

    this.article_list.updateAndSort();
  }

  selectBrand(brand: string) {
    this.selected_brand = {
        code: brand,
        onSell_amount: 0,
        pos_amount: 0,
    };
    this.selected_brand.code = brand;
    this.setArticleList();
    this.nextState();
  }

  addArticle(article: any) {
    if (article.qty_requested > 0) {
      if (!this.selected_articles.hasOwnProperty(this.selected_brand.code)) {
        this.selected_articles[this.selected_brand.code] = {
          articles: {},
          stats: {}
        };
      }
      this.selected_articles[this.selected_brand.code].articles[article.code] = article;
    } else {
      delete this.selected_articles[this.selected_brand.code].articles[article.code];
      if (!Object.keys(this.selected_articles[this.selected_brand.code].articles).length) {
        delete this.selected_articles[this.selected_brand.code];
        return;
      }
    }
    this.calculateBrandStats(this.selected_brand.code);
  }

  calculateBrandStats(brand_name: string) {
    const brand = this.selected_articles[brand_name];
    const classes = {};
    brand.stats = {
      articles: 0,  // articles quantity
      total: 0,  // total price for all band articles
      discount: 0,  // amount of discount
      pos: 0,  // promotional articles quantity
      free_pos: 0,  // promo articles quantity for free

      onSell_amount: 0, //total price of OnSell articles
      onSell_qty: 0,
      pos_amount: 0,    //total price of POS articles
    };

    Object.keys(brand.articles).forEach(code => {
      const article = brand.articles[code];
      const price = Math.floor(article.price * 100);
      brand.stats.total += price * article.qty_requested;
      brand.stats.articles += article.qty_requested;
      if (!classes.hasOwnProperty(article.class)) {
        classes[article.class] = {
          'onSell': 0,
          'pos': { quantity: 0, cheapest: undefined },
          'outOfStock': 0
        };
      }

      if (article.category == 'onSell') {
        classes[article.class].onSell   += article.qty_requested;
        brand.stats.onSell_qty          += article.qty_requested;
        brand.stats.onSell_amount       += price * article.qty_requested;

      } else if (article.category == 'pos') {
        brand.stats.pos                += article.qty_requested;
        brand.stats.pos_amount += price * article.qty_requested;

        //Avoid DISCOUNT calculation
        /*
        if (classes[article.class].pos.cheapest) {
          if (
            price <= Math.floor(classes[article.class].pos.cheapest.price * 100)
            && article.qty_requested < classes[article.class].pos.cheapest.qty_requested
          ) {
            classes[article.class].pos.cheapest = article;
          }
        } else {
          classes[article.class].pos.cheapest = article;
        }
       */
      }
    });


    //Find cheapest articles
    /*
    Object.keys(classes).forEach(cls => {
      if (classes[cls].pos.cheapest) {
        brand.stats.free_pos += Math.floor(
          classes[cls].onSell / classes[cls].pos.cheapest.coefficient
        );
        brand.stats.discount += brand.stats.free_pos * classes[cls].pos.cheapest.price;
      }
    });
    */

    brand.stats.total         = (brand.stats.total - Math.floor(brand.stats.discount * 100)) / 100;
    brand.stats.onSell_amount = brand.stats.onSell_amount / 100;
    brand.stats.pos_amount    = brand.stats.pos_amount / 100;

    this.calculateTotalPrice();
  }

  calculateTotalPrice() {
    this.order.total = 0;
    this.order.totalQty = 0;

    this.order.stats.onSell_qty = 0;
    this.order.stats.onSell_amount = 0;
    this.order.stats.pos_qty = 0;
    this.order.stats.pos_amount = 0;

    Object.keys(this.selected_articles).forEach(brand => {
      this.order.total += this.selected_articles[brand].stats.total * 100;
      this.order.totalQty += this.selected_articles[brand].stats.articles;

      this.order.stats.onSell_amount += this.selected_articles[brand].stats.onSell_amount * 100;
      this.order.stats.onSell_qty += this.selected_articles[brand].stats.onSell_qty;

      this.order.stats.pos_amount += this.selected_articles[brand].stats.pos_amount * 100;
      this.order.stats.pos_qty += this.selected_articles[brand].stats.pos;

    });

    if (this.order.total) {this.order.total = this.order.total / 100; }
    if (this.order.totalQty) {this.order.totalQty = this.order.totalQty; }


    if (this.order.stats.onSell_amount) {this.order.stats.onSell_amount = this.order.stats.onSell_amount / 100; }
    if (this.order.stats.pos_amount) {this.order.stats.pos_amount = this.order.stats.pos_amount / 100; }

    console.log(this.selected_articles);
  }


  //Shows a summary of all the selected articles
  showCartSummary() {
    this.selected_articles_summary = [];

    // convert articles to array
    Object.keys(this.selected_articles).forEach(brand => {
      const articles = this.selected_articles[brand].articles;

      this.selected_articles_summary.push({ 'brandName': brand,
                                           'articles':  Object.keys(articles).map(code => articles[code]) });

    });

  }

  saveOrder() {

    //show "waiting page"
    this.nextState();

    // convert articles to array
    Object.keys(this.selected_articles).forEach(brand => {
      const articles = this.selected_articles[brand].articles;
      this.selected_articles[brand].articles = Object.keys(articles).map(code => articles[code]);
    });
    this.order.articles = this.selected_articles;
    this.order.pendingArticlesToDelete = this.pending_articles_to_delete;

    /* this.orderService.saveOrder(this.order)
    //   .then(order => {
    //     this.toastr.success('l\'ordine è stato salvato con successo', 'Ordine salvato');

    //     //TODO: go to order buffer page (or to buffer of orders that are not on APP yet)
    //     // this.router.navigate(['/order/detail/' + order.id])

    //   })
    //   .catch(error => console.log('ERROR WHILE SAVING ORDER: ', error));
    */
    ///////////////////////////

    console.log('++++ ORDER ++++');
    console.log(this.order);
    console.log(JSON.stringify(this.order));

    const deviceOnline = window.navigator.onLine;

    //If online, store on the remote server
    if (deviceOnline === true) {
      this.orderService.saveOrderToServer(this.order).subscribe(
        result => {
          if (result.status == 201) { this.toastr.success('I dati del nuovo ordine sono stati salvati sul server.', 'Ordine salvato');
          } else {                    this.toastr.error('Errore durante l\'invio dell\'ordine al server. \
                                                       Risposta non riconosciuta', 'Errore'); }

          //Return to blank page
          this.router.navigate(['/dashboard']);
        },
        error => {
          this.toastr.error('Errore durante la connessione con il server.', 'Impossibile salvare i dati');
          console.log(error);
        });

    //Else store locally
    } else {
      this.toastr.warning('I dati saranno salvati su questo dispositivo in attesa che la connessione internet venga ristabilita.', 'Dispositivo offline');

      this.orderService.storeOnBuffer(this.order)
      .then(() => {
        this.router.navigate(['/dashboard']);
      })
      .catch(error => {
        console.log('ERROR WHILE SAVING CUSTOMER: ', error)
        this.toastr.error('Impossibile salvare i dati nel buffer del dispositivo.', 'ERRORE!');
      });
    }



  }

  filterArticles(key: string): void {
    // filtering elements by hiding them
    // because it's more simple way to persist each article state
    key = key.toLowerCase();
    const collection = document.getElementsByClassName('article-item');
    for (let i = 0; i < collection.length; i++) {
      const el = collection[i].getElementsByClassName('description-text')[0];
      if (key && el.innerHTML.toLowerCase().indexOf(key) === -1) {
        collection[i].setAttribute('hidden', 'true');
      } else {
        collection[i].removeAttribute('hidden');
      }
    }
  }

  goToState2() {
    const customerCouchID = this.customerAutocomplete.value.doc._id;
    this.selectCustomer(customerCouchID);
  }


  selectCustomer(customerCouchID) {
    this.order.customer_couchId = customerCouchID;
    this.brands.prev = [];
    this.brands.new = [];
    this.articles_for_confirmation = {};
    this.pending_articles = [];
    // this.catalog = JSON.parse(JSON.stringify(this.clean_catalog));
    this.selected_articles = {};
    this.prev_articles = {};

    this.customerService.fetchCustomer(this.order.customer_couchId)
      .then(customer => {

        this.selected_customer = customer;
        this.order.customerCode = customer.customerCode;
        this.order.customerName = customer.name;
        const customerArticles = customer;
        //this.getCustomerPendingOrders(customer);

        this.prev_articles = customer.orderedArticles;

        //Iterate pending articles
        if (customerArticles.pendingArticles.length > 0 ){
          customerArticles.pendingArticles.forEach(art => {
            console.log('article :' + art.artCode + ',' + art.qty);
            this.brands.all.forEach(brand => {

              if (this.catalog[brand.brandCode]) {
                this.article_categories.forEach(cat => {

                  if (this.catalog[brand.brandCode].hasOwnProperty(cat.code)) {
                    this.catalog[brand.brandCode][cat.code].forEach(article => {

                      if (article.code === art.artCode) {
                        // if (!this.prev_articles[brand.brandCode]) this.prev_articles[brand.brandCode] = [];
                        if (!this.articles_for_confirmation[brand.brandCode]) {this.articles_for_confirmation[brand.brandCode] = {}; }

                        console.log('mapping:' + brand.brandCode + '-' + art.artCode + '-' + art.qty);
                        const pendingArticle = new Product();
                        pendingArticle.desc = article.desc;
                        pendingArticle.code = art.artCode;
                        pendingArticle.qty_requested = Number(art.qty);
                        pendingArticle.price = article.price;
                        pendingArticle.class = article.class;

                        // this.prev_articles[brand.brandCode].push(art.artCode);
                        if (this.articles_for_confirmation[brand.brandCode][art.artCode]) {
                          this.articles_for_confirmation[brand.brandCode][art.artCode]
                            .qty_requested += pendingArticle.qty_requested;
                        } else {
                          this.articles_for_confirmation[brand.brandCode][art.artCode] = pendingArticle;
                        }
                      }
                    });
                  }
                });
              }
            });
          });
        }

        console.log('+++ Article for confirmation +++');
        console.log(this.articles_for_confirmation);

        // separate brands on previously ordered and new for customer
        this.brands.all.forEach(brand => {

          if (customer.orderedBrands.indexOf(brand.brandCode) != -1) {
            this.brands.prev.push(brand);
          } else {
            this.brands.new.push(brand);
          }
        });

        this.nextState();

      })
      .catch(error => console.log('ERROR WHILE FETCHING CUSTOMER', error));

    // this.orderService.fetchCustomerOrders(this.order.customer_couchId).then(orders => {
    //   orders.docs.forEach(order => {
    //     Object.keys(order.articles).forEach(brand => {
    //
    //       if (!this.prev_articles[brand]) this.prev_articles[brand] = [];
    //       if (!this.articles_for_confirmation[brand]) this.articles_for_confirmation[brand] = {};
    //       order.articles[brand].articles.forEach(article => {
    //
    //         this.prev_articles[brand].push(article.code);
    //         if (this.articles_for_confirmation[brand][article.code]) {
    //           this.articles_for_confirmation[brand][article.code]
    //             .qty_requested += article.qty_requested;
    //         } else {
    //           this.articles_for_confirmation[brand][article.code] = article;
    //         }
    //       });
    //     });
    //   });
    //   // separate brands on previously ordered and new for customer
    //   this.brands.all.forEach(brand => {
    //     if (Object.keys(this.prev_articles).indexOf(brand) != -1) {
    //       this.brands.prev.push(brand);
    //     } else {
    //       this.brands.new.push(brand);
    //     }
    //   });
    // })
    //   .catch(error => console.log('ERROR WHILE FETCHING CUSTOMER ORDERS', error));

  }


  confirmArticle(e, brand, code) {
    this.articles_for_confirmation[brand][code].confirmed = e.checked;
  }


  deletePendingArticles() {
    Object.keys(this.articles_for_confirmation).forEach(brand => {
      this.selected_brand.code = brand;
      Object.keys(this.articles_for_confirmation[brand]).forEach(code => {
        const article = this.articles_for_confirmation[brand][code];
        if (article.confirmed) {
          delete article.confirmed;
          this.pending_articles_to_delete[article.code] = article.qty_requested;
        }
      });
    });
    this.nextState();

  }
  // confirmPreviousArticles() {
  //   Object.keys(this.articles_for_confirmation).forEach(brand => {
  //     this.selected_brand.code = brand;
  //     Object.keys(this.articles_for_confirmation[brand]).forEach(code => {
  //       const article = this.articles_for_confirmation[brand][code];
  //       if (article.confirmed) {
  //         delete article.confirmed;
  //         this.addArticle(article);
  //         this.confirmed_articles[article.code] = article.qty_requested;
  //       }
  //     });
  //   });
  //   this.nextState();
  // }

  // setShipmentDate(e) {
  //   this.order.shippingDatePreferred = e;
  // }

  // setShippingAddress(e) {
  //   this.order.shippingAddress = e.target.value;
  //   console.log(e.target.value);
  // }

  // setPaymentType(e) {
  //   this.order.paymentType = e.value;
  // }
}
