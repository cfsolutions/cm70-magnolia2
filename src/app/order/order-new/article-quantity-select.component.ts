import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';


@Component({
  selector: 'product-quantity-select',
  template: `
    <mat-card>
      <div class="description">
        <p class="description-text">{{ article.code }} - {{ article.desc }}</p>
        <p class="price">PF: &euro; {{ article.price }} PP: &euro; {{ article.price * 2 }}</p>
        <p>
          classe: {{ article.class }}
          <span *ngIf="article.coefficient">coeff: {{ article.coefficient }}</span>
        </p>
      </div>
      <div class="btn-block">
        <button (click)="removeProduct()">-</button>
        <input type="number" [(ngModel)]="article.qty_requested" (keyup)="update($event)">
        <button (click)="addProduct()">+</button>
      </div>
    </mat-card>
  `,
  styles: [`
    .mat-card {
      padding: 0;
      display: flex;
      justify-content: space-between;
      min-height: 80px;
      margin-bottom: 10px;
    }
    .description {
      align-self: center;
      padding: 10px;
    }
    .description p {
      margin: 4px 0;
    }
    .price {
      font-size: 12px;
      color: #67a1ec;
    }
    .btn-block {
      display: flex;
    }
    input {
      align-self: center;
      text-align: center;
      width :44px;
      border: none;
      border-bottom: 2px solid #3F51B5;
      margin: 5px;
      font-size: 24px;
    }
    button {
      border: none;
      width: 50px;
      background: #3F51B5;
      color: #fff;
      font-size: 24px;
    }
  `]
})
export class ArticleQuantitySelectComponent implements OnInit {

  @Input() article: any;
  @Input() article_cat: string;
  @Output() quantityChanged = new EventEmitter<any>();

  rx_article = new Subject();

  ngOnInit() {
    if (this.article.qty_requested == undefined) this.article.qty_requested = 0;
    if (this.article_cat) this.article.category = this.article_cat;
    this.rx_article.debounceTime(500).subscribe(
      () => this.quantityChanged.emit(this.article)
    ) 
  }

  addProduct() {
    this.article.qty_requested++;
    this.rx_article.next();

  }
  removeProduct() {
    if (this.article.qty_requested > 0) {
      this.article.qty_requested--;
      this.rx_article.next();
    }
  }

  update($event) {
    console.log(this.article);
    this.rx_article.next();
  }


}