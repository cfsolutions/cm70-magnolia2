// import { Customer } from '../customer/customer';
// export class Order {
//     customer_id: string;    //CouchDB ID
//     customerCode: string;
//     payment_type: string;
//     shipmet_address: string;
//     preferred_shipment_date: Date;
//     upload_date: Date
//     articles: Article[];
//     total: number;

//     customer: Customer; userId: string;
//     addedOffline: boolean;

//     constructor(

//     ) { }
// }

// export class Article {
//     artCode: string;
//     artDesc: string;
//     qtyOrdered: number;
//     unitPrice: number;

//     constructor(

//     ) { }
// }




export class BrandList {
  brandArray: { brandCode: string, brandName: string }[];

  constructor(brandlist){
    this.brandArray = brandlist;
  }
  getBrandByName(brandName){
    return this.brandArray.filter( brand=> brand.brandName == brandName)[0];
  }
  getBrandByCode(brandCode){
    return this.brandArray.filter( brand=> brand.brandCode == brandCode)[0];
  }
}
