import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, ReactiveFormsModule} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';

import { OrderService } from '../order.service';
// import { Order, Article } from '../order';
import { CustomerService } from '../../customer/customer.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})

export class OrderListComponent implements OnInit {
  //customerFilter: string;
  orders = [];
  ordersOnline = [];
  filtertotalItems: any = { count: 0 };
  customers = [];
  selectedCustomer: any;
  selectedStatus: 'ALL';
  selectedBrand: 'ALL';
  startDate = new Date();
  endDate = new Date();
  catalog = [];
  clean_catalog = [];
  brands = { all: [], prev: [], new: [] };
  searchTitleHtml = '';

  customerAutocomplete = new FormControl();
  customersAutocompleteFilteredOptions: Observable<string[]>;

  constructor(private orderService: OrderService, private customerService: CustomerService,
    private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.fetchAllOrders();
    this.fetchArticles();
    this.fetchCustomers();
    // this.getOrderList();

    this.startDate = new Date(this.endDate.getFullYear(), this.endDate.getMonth(), 1);
  }

  fetchAllOrders() {
    this.orderService.fetchAllOrders()
      .then(response => { debugger
        this.orders = response.rows.slice();
        if (this.orders[0].id.includes('_design')) {
          this.orders.splice(0, 1);
        }
        // this.orders.forEach(element => {
        //   if (element.doc.customer_id !== undefined && element.doc.customer_id !== '') {
        //     this.customerService.fetchCustomer(element.doc.customer_id).then(result => {
        //       console.log(result);
        //       element.doc.customerName = result.name;
        //     })
        //       .catch(error => console.log(error));
        //   }
        // });
        console.log(this.orders);
      })
      .catch(error => console.log('ERROR WHILD FETCHING ORDERS: ', error));

  }

  /**
    * @param: onlineDatabase  if TRUE: search on the server
    */
  orderDetails(orderId: string, onlineDatabase: boolean) {
    let database = onlineDatabase ? 'online' : 'offline';
    this.router.navigate(['order/detail/' + database + '/', orderId]);
  }

  getOrderList() {
    this.orderService.getOrderList().subscribe(result => {
      if (result.status === 200) {
        console.log(result);
        this.ordersOnline = JSON.parse(result._body).orders;
        console.log(this.ordersOnline);
      }

    });
  }

  fetchArticles() {
    this.orderService.fetchArticles()
      .then(response => {
        let catalog = response.rows[0].doc.catalog;

        for (let brandId in catalog) {
            if (catalog.hasOwnProperty(brandId)) {
              this.brands.all.push({'brandId' : brandId, 'brandName': catalog[brandId].brandName});
                
            }
        }

        // this.brands.all = Object.keys(this.catalog).sort();
        // this.brands.all.unshift('All');
      })
      .catch(error => console.log('ERROR WHILE FETCHING ARTICLES', error));
  }

  fetchCustomers() {
    this.customerService.fetchAll()
      .then(response => {
        this.customers = response.rows
        this.setCustomersAutocomplete();
      })
      .catch(error => console.log('ERROR WHILE FETCHING CUSTOMERS: ', error));
  }

  selectBrand(event: any, option: any) {
    if (event.source.selected) {
      this.selectedBrand = option;
    }
  }

  searchOrders() {
    const start = this.startDate.toISOString().split('T');
    const end = this.endDate.toISOString().split('T');
    const obj = {
      // 'customerId': this.selectedCustomer,
      // 'brand': this.selectedBrand,
      'startDate': start[0],
      'endDate': end[0],
      // 'status': this.selectedStatus
    };

    //Clear previous result
    this.filtertotalItems.count = '';
    this.ordersOnline = [];
    this.searchTitleHtml = 'attendere...';


    if(this.selectedCustomer)          {obj['customerId'] = this.selectedCustomer.doc.customerCode;}
    if(this.selectedBrand != 'ALL')     {obj['brand'] = this.selectedBrand;}
    if(this.selectedStatus != 'ALL')    {obj['orderState'] = this.selectedStatus;}

    
    this.orderService.getOrderSearch(obj).subscribe(
      result => {
        if (result.status === 200) {
          console.log(result);
          const list = JSON.parse(result._body).orders;
          this.filtertotalItems.count = list.length;
          this.ordersOnline = list;
          this.searchTitleHtml = 'Totale ordini: ' + list.length;

        }
      },
      error => {
        this.toastr.error('Impossibile effettuare la ricerca.');
        this.searchTitleHtml = 'Impossibile effettuare la ricerca';
      });
  }

    ///// Customer Autocomplete
  setCustomersAutocomplete(){
    this.customersAutocompleteFilteredOptions = this.customerAutocomplete.valueChanges
      .pipe(
        startWith(''),
        map(val => val ? this.customersAutocompleteFilter(val) : this.customers.slice())
      );

  }

  customersAutocompleteFilter(val): string[] {
    return this.customers.filter(option =>{
      if(val !== undefined && typeof val === 'string') {
          return option.doc.name.toLowerCase().startsWith(val.toLowerCase());
      }
    });
  }

  autocompleteDisplayCustomerName(customer?): string | undefined {
    return customer ? customer.doc.name : undefined;
  }
  ///////////////////////////////////

}
