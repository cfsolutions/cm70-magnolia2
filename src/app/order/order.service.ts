import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchdbFind from 'pouchdb-find';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { genId } from '../utils';
// import { Order } from './order';


@Injectable()
export class OrderService {
  orderTable: string;
  customerTable: string;
  private db: PouchDB;
  private catalogDB: PouchDB;
  private ordersLocalBufferDB: PouchDB;
  private customersLocalBufferDB: PouchDB;

  private apiurl = environment.apiEndPoint;
  private remoteApiurl = environment.remoteCouchDB;  
  name:any;
  
  constructor(private http: Http) {

    this.customerTable=localStorage.getItem('customer_table');
    this.orderTable=localStorage.getItem('order_table');
    this.ordersLocalBufferDB = new PouchDB('ordersLocalBuffer');
    
    let PdbWithFind = PouchDB.plugin(PouchdbFind);
    this.db = new PdbWithFind(this.orderTable);
    this.name=this.orderTable;
    this.db.createIndex({ index: { fields: ['customer_id'] } });
 
    this.catalogDB = new PdbWithFind('catalog');

    this.remoteApiurl = environment.remoteCouchDB + this.orderTable;
    this.apiurl= environment.apiEndPoint+"orders"
  }
  

  /**
    * Store order request on the local device 
    */
  storeOnBuffer(order): Promise<any> {
    // customer.customerSince = new Date().toISOString();
    order.addedOffline = true;
    console.log(order);
    return this.ordersLocalBufferDB.post(order);
  }

  fetchBuffer(): Promise<any> {
    return this.ordersLocalBufferDB.allDocs({
      include_docs: true,
      sort: ['date']
    });
  }

  fetchArticles(): Promise<any> {
    // return Promise.resolve(CATALOG);
    return this.catalogDB.allDocs({
      include_docs: true
    });
  }



  //Push the order to the server
  saveOrderToServer(order): Observable<Response> {
    // order._id = genId();
    // order.addedOffline = true;
    // order.userId = localStorage.getItem('user_id')
    // order.upload_date = new Date().toISOString();
    // return this.db.put(order);

    var username = localStorage.getItem('username');
    var password = localStorage.getItem('password');

    const headers = new Headers();
    headers.append("Authorization", "Basic " + btoa(username + ":" + password)); 
    debugger
    return this.http.post(this.apiurl, order, { headers: headers }).map((response: Response) => 
    { 
      console.log('New order response status: ' + response.status); 
      return response;
    })
    .catch(this.handleError)

  }

  fetchAllOrders(): Promise<any> {
    return this.db.allDocs({
      include_docs: true,
      descending: false,
    });
  }

  fetchOrder(id): Promise<any> {
    return this.db.get(id);
  }

  fetchOnlineOrder(riferimentoMagnolia): Observable<any> {
    var username = localStorage.getItem('username');
    var password = localStorage.getItem('password');

    const headers = new Headers();
    headers.append("Authorization", "Basic " + btoa(username + ":" + password)); 

    return this.http.get(this.apiurl + '/' + riferimentoMagnolia, { headers: headers } ).map((response: Response) => <any>response)
      .catch(this.handleError)
  }


  fetchCustomerOrders(id): Promise<any> {
    return this.db.find({ selector: { customer_id: id } });
  }

  getOrderList(): Observable<any> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get(this.apiurl, { headers: headers }).map((response: Response) => <any>response)
      .catch(this.handleError)
  }

  getOrderSearch(obj: any): Observable<any> {
    var username = localStorage.getItem('username');
    var password = localStorage.getItem('password');

    const headers = new Headers();
    headers.append("Authorization", "Basic " + btoa(username + ":" + password)); 

    return this.http.post(this.apiurl + '/search',  obj, { headers: headers } ).map((response: Response) => <any>response)
      .catch(this.handleError)
  }

  // createNewOrder(obj: Order): Observable<any> {
  //   return this.http.post(this.apiurl + '/' , JSON.stringify(obj)).map((response: Response) => response)
  //     .catch(this.handleError)
  // }

  // deleteOrder(id:any)
  // {
  //   this.db.get(id).then(function(doc) {
  //     return this.db.remove(doc);
  //   }).then(function (result) {
  //     // handle result
  //   }).catch(function (err) {
  //     console.log(err);
  //   });
  // }

  deleteOrder(id:any, pouchDB:PouchDB) {
    
    return new Promise(
      function (resolve, reject) {
        pouchDB.get(id).then(function(doc) {
          return pouchDB.remove(doc);
        }).then(function (result) {
          pouchDB.compact();
          resolve();
        }).catch(function (err) {
          console.log(err);
          reject(err);
        });
    })
  }
  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
