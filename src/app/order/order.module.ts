import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { OrderNewComponent } from './order-new/order-new.component';
import { ArticleQuantitySelectComponent } from './order-new/article-quantity-select.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { OrderListComponent } from './order-list/order-list.component';
import { OrderService } from './order.service';
import { MaterialInputModule } from '../material-input.module';
import { LoggedInGuard } from '../login/logged-in.guard';
import { CustomerService } from '../customer/customer.service';

import { FormControl, ReactiveFormsModule} from '@angular/forms';


const moduleRoutes = [
  {
    path: 'order', children: [
      { path: 'new', component: OrderNewComponent, canActivate: [LoggedInGuard] },
      { path: 'list', component: OrderListComponent, canActivate: [LoggedInGuard] },
      { path: 'detail/:database/:id', component: OrderDetailComponent, canActivate: [LoggedInGuard] },
    ]
  }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(moduleRoutes),
    MaterialInputModule,
    ReactiveFormsModule,
  ],
  declarations: [
    OrderNewComponent,
    ArticleQuantitySelectComponent,
    OrderDetailComponent,
    OrderListComponent,
  ],
  exports: [
    OrderListComponent,
  ],
  providers: [OrderService, LoggedInGuard, CustomerService]
})
export class OrderModule { }
