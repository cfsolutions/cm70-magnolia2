module.exports = {
  "globDirectory": "dist/",
  "globPatterns": [
    "**/*.{txt,css,eot,svg,ttf,woff,woff2,otf,png,gif,ico,html,js}"
  ],
  "swDest": "dist/sw-workbox.js",
  "globIgnores": [
    "..//workbox-cli-config.js",
    "3rdpartylicenses.txt"
  ],

  skipWaiting: true,
  clientsClaim: true,
};