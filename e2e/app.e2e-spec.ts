import { Cm70Page } from './app.po';

describe('cm70 App', () => {
  let page: Cm70Page;

  beforeEach(() => {
    page = new Cm70Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
